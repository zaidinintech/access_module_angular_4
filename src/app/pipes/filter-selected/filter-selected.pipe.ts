import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSelected',
  pure: false
})
export class FilterSelectedPipe implements PipeTransform {

  transform(items: any[], filter: Object): any {
    if (!items || !filter) {
      return items;
    }

    return items.filter(item => !item['selected'] === !filter['selected']);
  }

}
