import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByText'
})
export class FilterByTextPipe implements PipeTransform {

  transform(items: any[], criteria: any[]): any {
    if (!items || !criteria) {
      return items;
    }

    if (criteria.filter(item => !!item).length < 2) {
      return items;
    }

    const property = criteria[0];
    const value = criteria[1];

    return items.filter(item => item[property].toLowerCase()
      .indexOf(value.toLowerCase()) !== -1);
  }

}
