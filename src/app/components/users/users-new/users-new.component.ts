import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserResource, UserModel } from '../../../resources/user.resource';
import { GroupModel, GroupResource } from '../../../resources/group.resource';


@Component({
  selector: 'app-users-new',
  templateUrl: './users-new.component.html',
  styleUrls: ['./users-new.component.css']
})
export class UsersNewComponent implements OnInit {

  newUser: UserModel;
  groups: GroupModel[];
  selectedGroupIds: String[];

  constructor(
    private userResource: UserResource,
    private groupResource: GroupResource,
    private router: Router
  ) { }

  ngOnInit() {
    this.groupResource.query()
    .$observable.subscribe(
      (recievedGroups: GroupModel[]) => {
        this.groups = recievedGroups;
      }
    );

    this.newUser = this.userResource.$createModel();
    this.selectedGroupIds = [];

  }

  saveUser() {
    this.newUser['groupIds'] = this.selectedGroupIds;

    this.userResource.save(this.newUser)
      .$observable.subscribe(
        () => {
          this.router.navigate(['/users']);
        }
      );
  }

  anyGroupPresent(): boolean {
    return this.groups && this.groups.length > 0 &&
      this.groups.length > this.selectedGroupIds.length;
  }

  anySelectedGroups(): boolean {
    return this.selectedGroupIds.length > 0;
  }

  toggleChoice(group) {
    group['chosen'] = !group['chosen'];
  }

  addSelectedGroup() {
    if (this.groups.length === this.selectedGroupIds.length ||
      this.groups.filter(group => group['chosen']).length < 1) {
        return;
    }

    // Keep track (ids) of selected groups
    this.selectedGroupIds = this.groups.filter(group => group['chosen'])
      .map(group => group['id']).concat(this.selectedGroupIds);

    this.groups.filter(group => !group['selected'])
      .forEach(group => {
        group['selected'] = group['chosen'];
        group['chosen'] = false;
      });

  }

  removeSelectedGroup() {
    if (this.selectedGroupIds.length < 1 ||
      this.groups.filter(group => group['chosen']).length < 1) {
        return;
    }

    // Get ids of de-selected groups
    const deSelectGroupIds = this.groups.filter(group => group['chosen'])
      .map(group => group['id']);

    // Get the Ids of the remaining selected groups.
    this.selectedGroupIds = this.selectedGroupIds.filter(
      groupId => deSelectGroupIds.indexOf(groupId) === -1
    );

    this.groups.filter(group => group['selected'])
      .forEach(group => {
        group['selected'] = !group['chosen'];
        group['chosen'] = false;
      });

  }

}
