import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';

import { UserModel, UserResource } from '../../../resources/user.resource';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users: UserModel[] = [];

  constructor(
    private userResource: UserResource,
    private router: Router
  ) { }

  ngOnInit(): any {
    this.userResource.query()
      .$observable.subscribe(
        (receivedUsers: UserModel[]) => {
          this.users = receivedUsers;
        }
      );
  }

  deleteUser(userToDelete) {
    this.userResource.remove({id: userToDelete.id})
      .$observable.subscribe(
        (deletedUser: UserModel) => {
          this.users = this.users.filter(user => user['id'] !== deletedUser['id']);
        }
      );
  }
}
