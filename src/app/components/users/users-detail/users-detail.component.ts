import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';
import 'rxjs/add/operator/switchMap';

import { UserModel, UserResource } from '../../../resources/user.resource';


@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.css']
})
export class UsersDetailComponent implements OnInit {

  user: UserModel;

  constructor(
    private userResource: UserResource,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.user = new UserModel();

    this.route.params
    .switchMap(
      (params: Params) => this.userResource.get({id: params['id']}).$observable
    )
    .subscribe(
      (receivedUser: UserModel) => {
        this.user = receivedUser;
      }
    );

  }

}
