import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';
import 'rxjs/add/operator/switchMap';

import { UserModel, UserResource } from '../../../resources/user.resource';
import { GroupModel, GroupResource } from '../../../resources/group.resource';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.css']
})
export class UsersEditComponent implements OnInit {

  userToBeEdited: UserModel;
  groups: GroupModel[];
  selectedGroupIds: String[];

  constructor(
    private userResource: UserResource,
    private groupResource: GroupResource,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.userToBeEdited = this.userResource.$createModel();

    this.route.params
    .switchMap(
      (params: Params) => this.userResource.get({id: params['id']}).$observable
    )
    .subscribe(
      (receivedUser: UserModel) => {
        this.userToBeEdited = receivedUser;
        this.selectedGroupIds = receivedUser['groups'].map(group => group.id);

        this.fetchGroups();
      }
    );

  }

  fetchGroups() {
    this.groupResource.query()
    .$observable.subscribe(
      (recievedGroups: GroupModel[]) => {
        this.groups = recievedGroups;

        this.groups.forEach(group => {
          if (this.selectedGroupIds.indexOf(group['id']) !== -1) {
            group['selected'] = true;
          }
        });
      }
    );
  }

  updateUser() {
    this.userToBeEdited['groupIds'] = this.selectedGroupIds;

    this.userResource.update(this.userToBeEdited)
      .$observable.subscribe(
        () => {
          this.router.navigate(['/users']);
        }
      );
  }

  anyGroupPresent(): boolean {
    return this.groups && this.groups.length > 0 &&
      this.groups.length > this.selectedGroupIds.length;
  }

  anySelectedGroups(): boolean {
    return this.selectedGroupIds.length > 0;
  }

  toggleChoice(group) {
    group['chosen'] = !group['chosen'];
  }

  addSelectedGroup() {
    if (this.groups.length === this.selectedGroupIds.length ||
      this.groups.filter(group => group['chosen']).length < 1) {
        return;
    }

    // Keep track (ids) of selected groups
    this.selectedGroupIds = this.groups.filter(group => group['chosen'])
      .map(group => group['id']).concat(this.selectedGroupIds);

    this.groups.filter(group => !group['selected'])
      .forEach(group => {
        group['selected'] = group['chosen'];
        group['chosen'] = false;
      });

  }

  removeSelectedGroup() {
    if (this.selectedGroupIds.length < 1 ||
      this.groups.filter(group => group['chosen']).length < 1) {
        return;
    }

    // Get ids of de-selected groups
    const deSelectGroupIds = this.groups.filter(group => group['chosen'])
      .map(group => group['id']);

    // Get the Ids of the remaining selected groups.
    this.selectedGroupIds = this.selectedGroupIds.filter(
      groupId => deSelectGroupIds.indexOf(groupId) === -1
    );

    this.groups.filter(group => group['selected'])
      .forEach(group => {
        group['selected'] = !group['chosen'];
        group['chosen'] = false;
      });

  }
}
