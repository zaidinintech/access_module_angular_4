import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { GroupResource, GroupModel } from '../../../resources/group.resource';


@Component({
  selector: 'app-groups-new',
  templateUrl: './groups-new.component.html',
  styleUrls: ['./groups-new.component.css']
})
export class GroupsNewComponent implements OnInit {

  newGroup: GroupModel;

  constructor(
    private groupResource: GroupResource,
    private router: Router
  ) { }

  ngOnInit() {
    this.newGroup = this.groupResource.$createModel();
  }

  saveGroup() {
    this.groupResource.save(this.newGroup)
      .$observable.subscribe(
        () => {
          this.router.navigate(['/groups']);
        }
      );
  }

}
