import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';
import 'rxjs/add/operator/switchMap';

import { GroupModel, GroupResource } from '../../../resources/group.resource';
import { DashboardModel, DashboardResource } from '../../../resources/dashboard.resource';

@Component({
  selector: 'app-groups-edit',
  templateUrl: './groups-edit.component.html',
  styleUrls: ['./groups-edit.component.css']
})
export class GroupsEditComponent implements OnInit {

  groupToBeEdited: GroupModel;
  dashboards: DashboardModel[];
  selectedDashboardIds: String[];

  constructor(
    private groupResource: GroupResource,
    private dashboardResource: DashboardResource,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.groupToBeEdited = this.groupResource.$createModel();

    this.route.params
    .switchMap(
      (params: Params) => this.groupResource.get({id: params['id']}).$observable
    )
    .subscribe(
      (receivedGroup: GroupModel) => {
        this.groupToBeEdited = receivedGroup;
        this.selectedDashboardIds = receivedGroup['dashboards'].map(dashboard => dashboard.id);

        this.fetchDashboards();
      }
    );
  }

  fetchDashboards() {
    this.dashboardResource.query()
    .$observable.subscribe(
      (recievedDashboards: DashboardModel[]) => {
        this.dashboards = recievedDashboards;

        this.dashboards.forEach(dashboard => {
          if (this.selectedDashboardIds.indexOf(dashboard['id']) !== -1) {
            dashboard['selected'] = true;
          }
        });
      }
    );
  }

  updateGroup() {
    this.groupToBeEdited['dashboardIds'] = this.selectedDashboardIds;

    this.groupResource.update(this.groupToBeEdited)
      .$observable.subscribe(
        () => {
          this.router.navigate(['/groups']);
        }
      );

  }

  anyDashboardPresent(): boolean {
    return this.dashboards && this.dashboards.length > 0 &&
      this.dashboards.length > this.selectedDashboardIds.length;
  }

  anySelectedDashboards(): boolean {
    return this.selectedDashboardIds.length > 0;
  }

  toggleChoice(dashboard) {
    dashboard['chosen'] = !dashboard['chosen'];
  }

  addSelectedDashboard() {
    if (this.dashboards.length === this.selectedDashboardIds.length ||
      this.dashboards.filter(dashboard => dashboard['chosen']).length < 1) {
        return;
    }

    // Keep track (ids) of selected dashboards
    this.selectedDashboardIds = this.dashboards.filter(dashboard => dashboard['chosen'])
      .map(dashboard => dashboard['id']).concat(this.selectedDashboardIds);

    this.dashboards.filter(dashboard => !dashboard['selected'])
      .forEach(dashboard => {
        dashboard['selected'] = dashboard['chosen'];
        dashboard['chosen'] = false;
      });

  }

  removeSelectedDashboard() {
    if (this.selectedDashboardIds.length < 1 ||
      this.dashboards.filter(dashboard => dashboard['chosen']).length < 1) {
        return;
    }

    // Get ids of de-selected dashboards
    const deSelectDashboardIds = this.dashboards.filter(dashboard => dashboard['chosen'])
      .map(dashboard => dashboard['id']);

    // Get the Ids of the remaining selected dashboards.
    this.selectedDashboardIds = this.selectedDashboardIds.filter(
      dashboardId => deSelectDashboardIds.indexOf(dashboardId) === -1
    );

    this.dashboards.filter(dashboard => dashboard['selected'])
      .forEach(dashboard => {
        dashboard['selected'] = !dashboard['chosen'];
        dashboard['chosen'] = false;
      });

  }
}
