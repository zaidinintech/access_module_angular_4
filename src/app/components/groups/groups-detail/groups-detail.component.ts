import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';
import 'rxjs/add/operator/switchMap';

import { GroupModel, GroupResource } from '../../../resources/group.resource';


@Component({
  selector: 'app-groups-detail',
  templateUrl: './groups-detail.component.html',
  styleUrls: ['./groups-detail.component.css']
})
export class GroupsDetailComponent implements OnInit {

  group: Observable<GroupModel>;

  constructor(
    private groupResource: GroupResource,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.group = this.route.params
    .switchMap(
      (params: Params) => this.groupResource.get({id: params['id']}).$observable
    );
  }

  goToGroupEdit(id) {
    this.router.navigate(['/groups/', id]);
  }

}
