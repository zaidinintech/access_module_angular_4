import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';

import { GroupModel, GroupResource } from '../../../resources/group.resource';


@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.css']
})
export class GroupsListComponent implements OnInit {

  groups: GroupModel[] = [];

  constructor(
    private groupResource: GroupResource,
    private router: Router
  ) { }

  ngOnInit(): any {
    this.groupResource.query()
      .$observable.subscribe(
        (receivedGroups: GroupModel[]) => {
          this.groups = receivedGroups;
        }
      );
  }

  deleteGroup(groupToDelete) {
    this.groupResource.remove({id: groupToDelete.id})
      .$observable.subscribe(
        (deletedGroup: GroupModel) => {
          this.groups = this.groups.filter(group => group['id'] !== deletedGroup['id']);
        }
      );
  }
}
