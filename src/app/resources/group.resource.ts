import { Injectable } from '@angular/core';
import { RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Resource, ResourceParams, ResourceModel, ResourceCRUD } from 'ngx-resource';

interface IGroupQuery {
  page?: number;
  perPage?: number;
}

interface IGroup {
  id?: string;
  name?: string;
  createdAt?: string;
  updatedAt?: string;
}

@Injectable()
@ResourceParams({
  url: 'http://localhost:1337/groups'
})
export class GroupResource extends ResourceCRUD<IGroupQuery, GroupModel, GroupModel> {}

export class GroupModel extends ResourceModel<GroupResource> implements IGroup {}

