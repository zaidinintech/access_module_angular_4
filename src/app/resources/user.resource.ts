import { Injectable } from '@angular/core';
import { RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Resource, ResourceParams, ResourceModel, ResourceCRUD } from 'ngx-resource';

interface IUserQuery {
  page?: number;
  perPage?: number;
}

interface IUser {
  id?: string;
  email?: string;
  name?: string;
  username?: string;
  createdAt?: string;
  updatedAt?: string;
}

@Injectable()
@ResourceParams({
  url: 'http://localhost:1337/users'
})
export class UserResource extends ResourceCRUD<IUserQuery, UserModel, UserModel> {}

export class UserModel extends ResourceModel<UserResource> implements IUser {}

