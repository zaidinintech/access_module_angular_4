import { Injectable } from '@angular/core';
import { RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Resource, ResourceParams, ResourceModel, ResourceCRUD } from 'ngx-resource';

interface IDashboardQuery {
  page?: number;
  perPage?: number;
}

interface IDashboard {
  id?: string;
  name?: string;
  createdAt?: string;
  updatedAt?: string;
}

@Injectable()
@ResourceParams({
  url: 'http://localhost:1337/dashboards'
})
export class DashboardResource extends ResourceCRUD<IDashboardQuery, DashboardModel, DashboardModel> {}

export class DashboardModel extends ResourceModel<DashboardResource> implements IDashboard {}

