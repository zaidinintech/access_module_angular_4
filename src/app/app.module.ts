import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResourceModule } from 'ngx-resource';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { BaseComponent } from './components/base/base.component';

import { UsersListComponent } from './components/users/users-list/users-list.component';
import { UsersDetailComponent } from './components/users/users-detail/users-detail.component';
import { UsersNewComponent } from './components/users/users-new/users-new.component';
import { UsersEditComponent } from './components/users/users-edit/users-edit.component';

import { GroupsListComponent } from './components/groups/groups-list/groups-list.component';
import { GroupsDetailComponent } from './components/groups/groups-detail/groups-detail.component';
import { GroupsNewComponent } from './components/groups/groups-new/groups-new.component';
import { GroupsEditComponent } from './components/groups/groups-edit/groups-edit.component';

import { FilterSelectedPipe } from './pipes/filter-selected/filter-selected.pipe';
import { FilterByTextPipe } from './pipes/filter-by-text/filter-by-text.pipe';

const appRoutes: Routes = [
  { path: 'users', component: UsersListComponent },
  { path: 'users/new', component: UsersNewComponent },
  { path: 'users/:id', component: UsersDetailComponent },
  { path: 'users/edit/:id', component: UsersEditComponent },
  { path: 'groups', component: GroupsListComponent },
  { path: 'groups/new', component: GroupsNewComponent },
  { path: 'groups/:id', component: GroupsDetailComponent },
  { path: 'groups/edit/:id', component: GroupsEditComponent },
];

@NgModule({
  declarations: [
    BaseComponent,
    // Users
    UsersListComponent,
    UsersDetailComponent,
    UsersNewComponent,
    UsersEditComponent,
    // Groups
    GroupsListComponent,
    GroupsDetailComponent,
    GroupsNewComponent,
    GroupsEditComponent,
    FilterSelectedPipe,
    FilterByTextPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    ResourceModule.forRoot()
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [],
  bootstrap: [BaseComponent]
})
export class AppModule { }
