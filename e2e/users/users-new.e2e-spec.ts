import { UsersIntegration } from './users.po';

describe('user-management User New page', () => {
  let page: UsersIntegration;

  beforeEach(() => {
    page = new UsersIntegration();
    page.navigateTo('/users/new');
  });

  it('displays "New User" at page', () => {
    expect(page.getText('.tile .tile-header h1')).toEqual('New User');
  });

  it('creates a user with name, username and email', () => {
    let oldCount = 0;

    page.navigateTo('/users');
    page.totalElements('//table[@class="table"]/tbody/tr')
      .count().then(
        (result) => {
          oldCount = result;
        }
      );

    page.navigateTo('/users/new');

    page.fillField('//input[@name="name"]', 'Abc');
    page.fillField('//input[@name="username"]', 'abc');
    page.fillField('//input[@name="email"]', 'abc@abc.com');
    page.clickButton('//button[@type="submit"]').then(() => {

      page.urlIs('/users');

      const newCount = page.totalElements('//table[@class="table"]/tbody/tr').count();
      expect(newCount).toEqual(oldCount + 1);

    });


  });

});
