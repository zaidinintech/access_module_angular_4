import { browser, by, element } from 'protractor';

export class UsersIntegration {

  BASE_URL = 'http://localhost:49152';

  navigateTo(path: string) {
    return browser.get(path);
  }

  getText(selector: string) {
    return element(by.css(selector)).getText();
  }

  totalElements(selector: string) {
  	return element.all(by.xpath(selector));
  }

  doesElementExist(selector: string) {
  	return element(by.xpath(selector)).isPresent();
  }

  fillField(name: string, text :string) {
  	return element(by.xpath(name)).sendKeys(text);
  }

  clickButton(selector: string) {
  	return element(by.xpath(selector)).click()
  }

  urlIs(url: string) {
    const EC = browser.ExpectedConditions;;
    return browser.wait(EC.urlIs(this.BASE_URL + url), 3000);
  }
}
