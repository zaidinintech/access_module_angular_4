import { UsersIntegration } from './users.po';

describe('user-management User List page', () => {
  let page: UsersIntegration;

  beforeEach(() => {
    page = new UsersIntegration();
    page.navigateTo('/users');
  });

  it('displays "List of Users" at page', () => {
    expect(page.getText('.tile .tile-header h1')).toEqual('List of Users');
  });

  it('has a "New User" button', () => {
  	expect(page.doesElementExist('//a[@routerlink="/users/new"]')).toEqual(true);
  });

  it('has a "Edit" button in actions for every user ', () => {
  	expect(page.doesElementExist('//table[@class="table"]//a[text()="Edit"]')).toEqual(true);
  });

  it('has a "Delete" button in actions for every user ', () => {
  	expect(page.doesElementExist('//table[@class="table"]//button[text()="Delete"]')).toEqual(true);
  });

});
