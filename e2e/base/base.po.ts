import { browser, by, element } from 'protractor';

export class UserManagementPage {
  navigateTo() {
    return browser.get('/');
  }

  getHeading() {
    return element(by.css('.pageheader h2')).getText();
  }
}
