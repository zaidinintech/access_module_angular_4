import { UserManagementPage } from './base.po';

describe('user-management Base', () => {
  let page: UserManagementPage;

  beforeEach(() => {
    page = new UserManagementPage();
  });

  it('should display "Dashboard" at the root', () => {
    page.navigateTo();
    expect(page.getHeading()).toEqual('Dashboard');
  });
});
